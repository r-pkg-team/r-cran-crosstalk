r-cran-crosstalk (1.2.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  * Update lintian override info format in d/source/lintian-overrides on line 2.

 -- Andreas Tille <tille@debian.org>  Sun, 26 Nov 2023 17:37:31 +0100

r-cran-crosstalk (1.2.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.2.0+dfsg
  * Add r-cran-sass to test-deps

 -- Nilesh Patra <nilesh@debian.org>  Sun, 07 Nov 2021 20:25:13 +0530

r-cran-crosstalk (1.1.1+dfsg-2) unstable; urgency=medium

  * Team Upload.
  * Move r-cran-shiny to Depends (Closes: #986011)

 -- Nilesh Patra <nilesh@debian.org>  Mon, 29 Mar 2021 19:06:25 +0530

r-cran-crosstalk (1.1.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Fri, 15 Jan 2021 09:30:46 +0100

r-cran-crosstalk (1.1.0.1+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Switch from fonts-glewlwyd to fonts-glyphicons-halflings (Closes: #962884)
  * debhelper-compat 13 (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 17 Jun 2020 07:47:49 +0200

r-cran-crosstalk (1.1.0.1+dfsg-2) unstable; urgency=medium

  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit,
    Repository, Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Andreas Tille <tille@debian.org>  Sun, 17 May 2020 21:36:28 +0200

r-cran-crosstalk (1.1.0.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Fri, 20 Mar 2020 09:02:38 +0100

r-cran-crosstalk (1.0.0+dfsg-4) unstable; urgency=medium

  * r-cran-shiny does not provide strftime.min.js any more - provide it
    here instead.
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Set upstream metadata fields: Contact, Name.

 -- Andreas Tille <tille@debian.org>  Mon, 19 Aug 2019 15:29:46 +0200

r-cran-crosstalk (1.0.0+dfsg-3) unstable; urgency=medium

  * Fix symlink
    Closes: #905283
  * Standards-Version: 4.2.0

 -- Andreas Tille <tille@debian.org>  Sat, 04 Aug 2018 12:15:22 +0200

r-cran-crosstalk (1.0.0+dfsg-2) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Tue, 05 Jun 2018 12:02:55 +0200

r-cran-crosstalk (1.0.0+dfsg-1) unstable; urgency=medium

  * Initial release (closes: #884851)

 -- Andreas Tille <tille@debian.org>  Wed, 20 Dec 2017 15:48:23 +0100
